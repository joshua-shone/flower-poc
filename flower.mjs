import * as path from './path.mjs';

customElements.define('iometer-flower', class Flower extends HTMLElement {
	constructor() {
		super();

		this.innerHTML = `
			<div class="shadow">
				<div data-metric="consumption"></div>
			</div>
			<div class="shadow">
				<div data-metric="stateOfCharge"></div>
			</div>
			<div class="shadow">
				<div data-metric="batteryCharging"></div>
			</div>
			<div class="shadow">
				<div data-metric="batteryDischarging"></div>
			</div>
			<div class="shadow">
				<div data-metric="gridFeedin"></div>
			</div>
			<div class="shadow">
				<div data-metric="gridPurchase"></div>
			</div>
			<div class="shadow">
				<div data-metric="solarTracker1"></div>
			</div>
			<div class="shadow">
				<div data-metric="solarTracker2"></div>
			</div>
			<div class="shadow">
				<div data-metric="solarProduction"></div>
			</div>
			<svg class="overlays" viewBox="-1.05 -1.05 2.1 2.1">
				<circle class="low-mark"   r=".56"/>
				<circle class="mid-mark"   r=".7"/>
				<circle class="high-mark"  r=".84"/>
				<circle class="outer-ring" r="1"/>
				<g class="outer-ring-markers"/>
				<circle class="now-marker" r=".04"/>
			</svg>
			<div class="top-circle">
				00:00
			</div>
		`;

		this.centerContainer = this.querySelector('.center-container');

		this.singleDayData = [];
		this.maxConsumption = null;

		// Create outer ring ticks/labels
		this.querySelector('.outer-ring-markers').innerHTML = [...Array(24)].map((v, i) => {
			const x = Math.sin(Math.PI - (Math.PI * 2 * (i/24)));
			const y = Math.cos(Math.PI - (Math.PI * 2 * (i/24)));
			if (i % 6 === 0) return `<text x="${x}" y="${y}">${i}</text>`;
			else             return `<circle cx="${x}" cy="${y}" r=".01"/>`;
		}).join('');

		window.addEventListener('resize', () => this.updateForSize());
	}

	enableMetric(metricName, enable) {
		this.querySelector(`[data-metric="${metricName}"]`).classList.toggle('enabled', enable);
	}

	update(data, endIndex, maxConsumption) {
		// 360 degrees covers one day, so iterate back through the data entries until one days worth of entries are found.
		let startIndex = endIndex;
		while (startIndex > 0 && (data[endIndex].t - data[startIndex].t) < 1) {
			startIndex--;
		}

		this.singleDayData = data.slice(startIndex, endIndex);
		this.maxConsumption = maxConsumption;

		this.updateForSize();

		if (this.singleDayData.length >= 2) {
			const lastEntryAngle = ((this.singleDayData[this.singleDayData.length-1].t % 1) * (Math.PI*2)) - 0.01;
			this.style.setProperty('--angle', `${lastEntryAngle}rad`);
			this.querySelector('.now-marker').setAttribute('cx', Math.sin(Math.PI - lastEntryAngle) * .9);
			this.querySelector('.now-marker').setAttribute('cy', Math.cos(Math.PI - lastEntryAngle) * .9);
		}

		this.querySelector('.top-circle').textContent = data[endIndex].timeString;
	}
	
	updateForSize() {
		const boundingRect = this.getBoundingClientRect();
		const size = Math.min(boundingRect.width, boundingRect.height);
		this.style.setProperty('--size', `${size}px`);
		const flowerRadius = size / 2;

		const metrics = [...this.querySelectorAll('[data-metric]')];
		for (const metric of metrics) {
			const metricName = metric.dataset.metric;

			const points = this.singleDayData.map(entry => {
				const angle = Math.PI - (entry.t % 1) * (Math.PI*2);
				const lowMark = 0.56;
				const amplitude = lowMark + ((1-lowMark) * (entry[metricName] / this.maxConsumption));
				return {
					x: flowerRadius + (Math.sin(angle) * amplitude * flowerRadius * .952),
					y: flowerRadius + (Math.cos(angle) * amplitude * flowerRadius * .952),
				}
			});
			metric.style.clipPath = `path('M${flowerRadius},${flowerRadius} ${path.makeSmooth(points)} z')`;
		}
	}
})
