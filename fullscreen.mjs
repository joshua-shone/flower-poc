import * as path from './path.mjs';

customElements.define('iometer-fullscreen-toggle', class FullscreenToggle extends HTMLElement {
	constructor() {
		super();
		this.onclick = this._onclick;
	}

	_onclick() {
		// Toggle full-screen mode. Note that on Safari fullscreen methods are prefixed with 'webkit'.
		if (!(document.fullscreenElement || document.webkitFullscreenElement)) {
			if (document.documentElement.requestFullscreen) document.documentElement.requestFullscreen({navigationUI: "hide"});
			else if (document.documentElement.webkitRequestFullscreen) document.documentElement.webkitRequestFullscreen();
		} else {
			if (document.exitFullscreen) document.exitFullscreen();
			else if (document.webkitExitFullscreen) document.webkitExitFullscreen();
		}
	}
});
