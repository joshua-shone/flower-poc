import * as path from './path.mjs';

customElements.define('iometer-linegraph', class Linegraph extends HTMLElement {
	constructor() {
		super();

		this.t = 0;

		this.singleDayData = [];
		this.maxConsumption = null;

		this.onscrub = t => {};

		this.range = .25; // The horizontal range of the line graph covers a quarter of a day
		this.markInterval = 1/24;
		this.timeLabelInterval = 1/12;

		this.scrollMomentum = 0;
		this.lastScrollMomentumTimestamp = 0;
		this.scrollMomentumFrameId = null;
		this.scrollMomentumThreshold = .00001;
		this.scrollDeceleration = 0.005;

		this.innerHTML = `
			<div class="now-label">
				<strong>0,00</strong> &nbsp; kWh
			</div>
			<div class="graph-area">
				<div class="shadow">
					<div data-metric="consumption"></div>
				</div>
				<div class="shadow">
					<div data-metric="stateOfCharge"></div>
				</div>
				<div class="shadow">
					<div data-metric="batteryCharging"></div>
				</div>
				<div class="shadow">
					<div data-metric="batteryDischarging"></div>
				</div>
				<div class="shadow">
					<div data-metric="gridFeedin"></div>
				</div>
				<div class="shadow">
					<div data-metric="gridPurchase"></div>
				</div>
				<div class="shadow">
					<div data-metric="solarTracker1"></div>
				</div>
				<div class="shadow">
					<div data-metric="solarTracker2"></div>
				</div>
				<div class="shadow">
					<div data-metric="solarProduction"></div>
				</div>
				<svg viewBox="0 0 1 1.2" preserveAspectRatio="none">
					<mask id="line-graph-clippath">
						<rect x="-1" y="-1" width="3" height="3" fill="black"/>
						<path class="mask-path" fill="white"/>
					</mask>
					<g class="marks" mask="url(#line-graph-clippath)"/>
					<line class="selection-line" x1=".5" y1="1" x2=".5" y2=".02"/>
				</svg>
				<div class="time-labels"></div>
			</div>
		`;

		window.addEventListener('resize', () => this.updateForSize());
		this.onpointerdown = this._onpointerdown;
	}

	enableMetric(metricName, enable) {
		this.querySelector(`[data-metric='${metricName}']`).classList.toggle('enabled', enable);
	}

	update(data, t, maxConsumption) {
		this.t = t;

		// Search through the data entries to find enough to fill the visible horizontal range (this.range).
		let startIndex = null;
		let endIndex = null;
		let selectedIndex = 0;
		for (let i=0; i<data.length; i++) {
			const deltaT = t - data[i].t;
			if (Math.abs(deltaT) <= .6) {
				if (startIndex === null) startIndex = i;
				endIndex = i;
			}
			if (deltaT > 0) {
				selectedIndex = i;
			}
		}

		this.querySelector('.now-label strong').textContent = (data[selectedIndex].consumption / 100).toFixed(2);

		const minT = t - (this.range / 2);
		const maxT = t + (this.range / 2);

		// Build marks
		const firstMarkT = minT - (minT % this.markInterval);
		const markCount = Math.ceil(this.range / this.markInterval) + 1;
		this.querySelector('.marks').innerHTML = [...Array(markCount)].map((v, i) => {
			const t = firstMarkT + (i * this.markInterval);
			const x = this.tToX(t);
			return `<line x1="${x}" x2="${x}" y1="0" y2="1"/>`;
		}).join('');

		// Build time labels
		const firstTimeLabelT = minT - (minT % this.timeLabelInterval);
		const timeLabelCount = Math.ceil(this.range / this.timeLabelInterval) + 1;
		this.querySelector('.time-labels').innerHTML = [...Array(timeLabelCount)].map((v, i) => {
			const t = firstTimeLabelT + (i * this.timeLabelInterval);
			const x = this.tToX(t);
			const hour = String(Math.floor((t % 1) * 24)).padStart(2, '0');
			const minute = String(Math.floor((t % (1/24)) * 60)).padStart(2, '0');
			return `<span style="left: ${x*100}%">${hour}:${minute}</span>`;
		}).join('');

		this.maxConsumption = maxConsumption;
		this.singleDayData = data.slice(startIndex, endIndex);
		if (this.singleDayData.length <= 1) {
			this.querySelector('.graph').setAttribute('d', '');
			return;
		}

		this.updateForSize();
	}

	tToX(t) {
		const minT = this.t - (this.range / 2);
		return (t - minT) / this.range;
	}

	updateForSize() {
		const graphAreaRect = this.querySelector('.graph-area').getBoundingClientRect();

		const metrics = [...this.querySelectorAll('[data-metric]')];
		for (const metric of metrics) {

			const metricName = metric.dataset.metric;
			
			const points = this.singleDayData.map(entry => {
				return {
					x: graphAreaRect.width * this.tToX(entry.t),
					y: (graphAreaRect.height - 35) * (1 - (entry[metricName] / this.maxConsumption)),
				}
			});
			const pathString = `path('M${points[0].x},${graphAreaRect.height} ${path.makeSmooth(points)} L${points[points.length-1].x},${graphAreaRect.height}')`;
			metric.style.clipPath = pathString;
			if (metricName === 'consumption') {
				this.querySelector('.mask-path').setAttribute('d', pathString);
			}
		}
	}

	_onpointerdown(event) {
		event.preventDefault();
		if (this.onpointermove) {
			return;
		}
		this.style.cursor = 'grabbing';

		const pointerId = event.pointerId;
		if (this.setPointerCapture) {
			this.setPointerCapture(pointerId);
		}

		this.scrollMomentum = 0;
		if (this.scrollMomentumFrameId !== null) {
			window.cancelAnimationFrame(this.scrollMomentumFrameId);
			this.scrollMomentumFrameId = null;
		}

		let lastScreenX = event.screenX;
		let movementX = 0;
		this.onpointermove = event => {
			movementX = event.screenX - lastScreenX;
			lastScreenX = event.screenX;
			const nearestMark = Math.round(this.t / this.markInterval) * this.markInterval;
			const prevT = this.t;
			this.t -= this.range * (movementX / this.getBoundingClientRect().width)
			if (navigator.vibrate) {
				if ((prevT < nearestMark && this.t >= nearestMark) || (prevT > nearestMark && this.t <= nearestMark)) {
					navigator.vibrate(20);
				}
			}
			this.onscrub(this.t);
		}
		this.onpointerup = event => {
			if (event.pointerId === pointerId) {
				this.onpointermove = null;
				this.onpointerup = null;
				this.style.cursor = null;
				this.lastScrollMomentumTimestamp = performance.now();
				this.scrollMomentum = this.range * (movementX / this.getBoundingClientRect().width) * -0.1;
				if (this.scrollMomentumFrameId === null && Math.abs(this.scrollMomentum) > this.scrollMomentumThreshold) {
					this.scrollMomentumFrameId = window.requestAnimationFrame(timestamp => this.scrollMomentumFrame(timestamp));
				} else {
					this.scrollMomentum = 0;
				}
			}
		}
	}

	scrollMomentumFrame(timestamp) {
		const delta = timestamp - this.lastScrollMomentumTimestamp;
		this.lastScrollMomentumTimestamp = timestamp;
		this.scrollMomentum *= Math.max(0, 1 - (delta * this.scrollDeceleration));
		const nearestMark = Math.round(this.t / this.markInterval) * this.markInterval;
		const prevT = this.t;
		this.t += this.scrollMomentum * delta
		this.t -= (this.t - nearestMark) * delta * .01;
		if (Math.abs(this.scrollMomentum) < .00001 && Math.abs(this.t - nearestMark) < .001) {
			this.t = nearestMark;
		} else {
			this.scrollMomentumFrameId = window.requestAnimationFrame(timestamp => this.scrollMomentumFrame(timestamp));
		}
		if ((prevT < nearestMark && this.t >= nearestMark) || (prevT > nearestMark && this.t <= nearestMark)) {
			navigator.vibrate(50);
		}
		this.onscrub(this.t);
	}
});
