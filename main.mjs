if (navigator.serviceWorker) {
	// Register service worker so app can be installed as a PWA
	navigator.serviceWorker.register('service-worker.js', {scope: './'});
}

window.onbeforeinstallprompt = event => {
	event.preventDefault();
	const installButton = document.querySelector('#top-bar .install')
	installButton.classList.remove('hidden');
	installButton.onclick = () => {
		event.prompt();
	}
	event.userChoice.then(result => {
		if (result.outcome === 'accepted') {
			installButton.remove();
		}
	});
}

const metricSelector = document.getElementById('metric-selector');
const flower = document.querySelector('iometer-flower');
const linegraph = document.querySelector('iometer-linegraph');

loadCsv('202002.csv');

async function loadCsv(filename) {
	const response = await fetch(`data/${filename}`);
	const csv = await response.text();

	const lines = csv.split('\n');
	const fieldNames = lines[0].split(';');
	const fieldIndices = {
		timestamp:          fieldNames.findIndex(field => field == 'timestamp'),
		stateOfCharge:      fieldNames.findIndex(field => field == 'Ladezustand'),
		batteryCharging:    fieldNames.findIndex(field => field == 'Batterie (Laden)'),
		batteryDischarging: fieldNames.findIndex(field => field == 'Batterie (Entladen)'),
		gridFeedin:         fieldNames.findIndex(field => field == 'Netzeinspeisung'),
		gridPurchase:       fieldNames.findIndex(field => field == 'Netzbezug'),
		solarTracker1:      fieldNames.findIndex(field => field == 'Solarproduktion Tracker 1'),
		solarTracker2:      fieldNames.findIndex(field => field == 'Solarproduktion Tracker 2'),
		solarProduction:    fieldNames.findIndex(field => field == 'Solarproduktion'),
		consumption:        fieldNames.findIndex(field => field == 'Hausverbrauch'),
	}

	// Parse the CSV data
	const data = lines.slice(1).map(line => {
		const fields = line.split(';');
		return {
			t: parseDatetime(fields[fieldIndices.timestamp]), // 't' is in decimal-day units, e.g. midday on the second day would be 2.5
			timeString:         fields[fieldIndices.timestamp].split(' ')[1],
			stateOfCharge:      parseInt(fields[fieldIndices.stateOfCharge]),
			batteryCharging:    parseInt(fields[fieldIndices.batteryCharging]),
			batteryDischarging: parseInt(fields[fieldIndices.batteryDischarging]),
			gridFeedin:         parseInt(fields[fieldIndices.gridFeedin]),
			gridPurchase:       parseInt(fields[fieldIndices.gridPurchase]),
			solarTracker1:      parseInt(fields[fieldIndices.solarTracker1]),
			solarTracker2:      parseInt(fields[fieldIndices.solarTracker2]),
			solarProduction:    parseInt(fields[fieldIndices.solarProduction]),
			consumption:        parseInt(fields[fieldIndices.consumption]),
		}
	});
	const maxConsumption = Math.max(...data.map(entry => entry.consumption));

	let selectedT;

	let oneDayIndex = 0;
	while (oneDayIndex < data.length && (data[oneDayIndex].t - data[0].t) <= 1) {
		oneDayIndex++;
	}
	setSelectedT(data[oneDayIndex].t);

	linegraph.onscrub = setSelectedT;

	function setSelectedT(t) {
		if (selectedT === t) {
			return;
		}
		selectedT = t;

		if (selectedT < data[0].t) selectedT = data[0].t;
		else if (selectedT >= data[data.length-1].t) selectedT = data[data.length-1].t;

		let flowerIndex = 0;
		while (flowerIndex<data.length-1 && (Math.abs(data[flowerIndex+1].t-selectedT) < Math.abs(data[flowerIndex].t-selectedT))) {
			flowerIndex++;
		}

		flower.update(data, flowerIndex, maxConsumption);
		linegraph.update(data, selectedT, maxConsumption);
	}

	for (const metric of metricSelector.querySelectorAll('[data-metric]')) {
		if (metric.classList.contains('enabled')) {
			flower.enableMetric(metric.dataset.metric, true);
			linegraph.enableMetric(metric.dataset.metric, true);
		}
	}
	metricSelector.onclick = event => {
		const metric = event.target.closest('[data-metric]');
		if (metric) {
			const enabled = metric.classList.toggle('enabled');
			flower.enableMetric(metric.dataset.metric, enabled);
			linegraph.enableMetric(metric.dataset.metric, enabled);
		}
	}
}

function parseDatetime(str) {
	// Parse a date/time string from the CSV as a decimal day, e.g. midday on the second day of the month would be 2.5
	const [date, time] = str.split(' ');
	const [year, month, day] = date.split('-');
	const [hour, second] = time.split(':');
	return parseInt(day) + (parseInt(hour) / 24) + (parseInt(second) / (24 * 60));
}


