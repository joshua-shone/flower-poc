self.addEventListener('install', event => {
	event.waitUntil(async function installWaitUntil() {
		// Cache everything the app should need to run offline
		const cache = await caches.open('static-assets-v1');
		await cache.addAll([
			'./',
			'./index.html',
			'./manifest.webmanifest',
			'./main.mjs',
			'./main.css',
			'./path.mjs',
			'./flower.mjs',
			'./flower.css',
			'./linegraph.mjs',
			'./linegraph.css',
			'./fullscreen.mjs',
			'./fullscreen.css',
			'./data/202002.csv',
			'./icons/icon_64x64.png',
			'./icons/icon_192x192.png',
			'./icons/icon_512x512.png',
		]);
	}());
});

self.addEventListener('fetch', event => {
	event.respondWith(async function fetchRespondWith() {
		const cache = await caches.open('static-assets-v1');
		try {
			const networkResponse = await fetch(event.request);
			cache.put(event.request, networkResponse.clone());
			return networkResponse;
		} catch (error) {
			return cache.match(event.request);
		}
	}());
});
